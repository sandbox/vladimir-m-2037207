
-- SUMMARY --

Print variables.

For a full description visit the project page:
  http://drupal.org/project/libraries
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/libraries


-- REQUIREMENTS --

* Libraries API. For a full description visit the project page:
  http://drupal.org/project/libraries


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
  Note that installing external libraries is separate from installing this
  module and should happen in the sites/all/libraries directory. See
  http://drupal.org/node/1440066 for more information.


-- CONTACT --

Current maintainer:
* Vladimir Melnic (vladimir-m) - http://drupal.org/user/1580452

